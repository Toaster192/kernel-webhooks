# Upstream commit ID comparison Webhook

## Purpose

This webhook compares backported commits against their upstream source, and identifies RHEL specific issues such as KABI changes.

## Manual runs

You can run the webhook manually on a merge request URL with the command:

    python3 -m webhook.commit_compare \
        --merge-request https://gitlab.com/group/repo/-/merge_requests/1 \
	--linux-src /usr/src/linux

The [main README](README.md#running-a-webhook-for-one-merge-request) describes
some common environment variables that can be set that are applicable for all
webhooks.

The `--linux-src` argument can also be passed to the script via the `LINUX_SRC`
environment variable.
